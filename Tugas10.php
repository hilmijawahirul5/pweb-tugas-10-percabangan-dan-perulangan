<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Input Nilai</title>
</head>
<body>
    <h2>Form Input Nilai</h2>
    <form action="Tugas10.php" method="post">
        <label for="nilai">Nilai:</label>
        <input type="number" id="nilai" name="nilai" required min="0" max="100" step="any"><br><br>
        <input type="submit" value="Submit">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $nilai = htmlspecialchars($_POST['nilai']);
    
        echo "<h2>Data Nilai</h2>";
        echo "Nilai: " . $nilai . "<br>";

        if ($nilai >= 80) {
            $grade = "A";
        } elseif ($nilai >= 76.25) {
            $grade = "A-";
        } elseif ($nilai >= 68.75) {
            $grade = "B+";
        } elseif ($nilai >= 65) {
            $grade = "B";
        } elseif ($nilai >= 62.50) {
            $grade = "B-";
        } elseif ($nilai >= 57.50) {
            $grade = "C+";
        } elseif ($nilai >= 55) {
            $grade = "C";
        } elseif ($nilai >= 51.25) {
            $grade = "C-";
        } elseif ($nilai >= 43.75) {
            $grade = "D+";
        } elseif ($nilai >= 40) {
            $grade = "D-";
        } elseif ($nilai <= 39.99) {
            $grade = "E";
        }
        echo "Grade: " . $grade . "<br>";
    }
    ?>
</body>
</html>