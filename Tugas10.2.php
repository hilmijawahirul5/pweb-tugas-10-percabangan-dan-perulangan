<?php

function cetakSegitiga($tinggi){
    for ($i = 1; $i <= $tinggi; $i++) {
        for ($k = $tinggi; $k > $i; $k--) {
            echo "&nbsp&nbsp&nbsp";
        }
        for ($j = 1; $j <= (2 * $i - 1); $j++) {
            echo "* ";
        }
        echo "<br>";
    }
}

$tinggi = 5;
cetakSegitiga($tinggi);

?>